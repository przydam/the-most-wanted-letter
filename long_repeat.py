def count_repeat(line: str) -> int:
    """
        length the longest substring that consists of the same char
    """
    # your code here
    return_number = 0
    for char in line:
        if line.count(char) > return_number:
            return_number = line.count(char)



    return return_number












def long_repeat(line: str) -> int:
    line = [char for char in line]
    old_element = ""
    return_number = 1
    return_number1 = 1
    for element in line:
        if old_element == element:
            return_number1 = return_number1 + 1
        else:
            return_number1 = 1
        old_element = element
        if return_number1> return_number:
            return_number = return_number1

    if not line:
        return_number = 0


    return return_number

        #These "asserts" using only for self-checking and not necessary for auto-testing
print(long_repeat('sdsffffse'))# == 4, "First"
print(long_repeat('ddvvrwwwrggg'))# == 3, "Second"
print(long_repeat('abababaab'))# == 2, "Third"
print(long_repeat(''))# == 0, "Empty"
