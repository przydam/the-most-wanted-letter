#Your optional code here
#You can import some modules or create additional functions


def checkio(data: list) -> list:
    #Your code here
    #It's main function. Don't remove this function
    #It's used for auto-testing and must return a result for check.

    #replace this for solution
    return [i for i in data if data.count(i) > 1]

#Some hints
#You can use list.count(element) method for counting.
#Create new list with non-unique elements
#Loop over original list



    #These "asserts" using only for self-checking and not necessary for auto-testing
print(checkio([1, 2, 3, 1, 3]))
print(checkio([1, 2, 3, 4, 5]))
print(checkio([5, 5, 5, 5, 5]))
print(checkio([10, 9, 10, 10, 9, 8]))
print("It is all good. Let's check it now")
