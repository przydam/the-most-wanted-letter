def sun_angle(time):
    time = time.split(":")
    hour = int(time[0])-6
    minute = 0
    if int(time[1]):
        minute = 60/int(time[1])
    if hour < 0 or (hour >= 12 and minute > 0):
        return "I don't see the sun!"
    else:
        if minute:
            minute = 15/minute
        return hour*15 + minute






print("Example:")
print(sun_angle("07:00"))

#These "asserts" using only for self-checking and not necessary for auto-testing
print(sun_angle("07:00"))# == 15
print(sun_angle("01:23"))# == "I don't see the sun!"
print(sun_angle("12:15"))
print(sun_angle("18:00"))
print(sun_angle("06:00"))
print(sun_angle("18:01"))