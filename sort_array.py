from collections import Counter
def frequency_sort(items):
    # your code here
    #result = [item for items, c in Counter(items).most_common()
     #         for item in [items] * c]

    ret = []
    set1 = set(items)
    list_wcnt = [(x, items.count(x), items.index(x)) for x in set1]
    sorted_list = sorted(list_wcnt, key=lambda x: (1 / x[1], x[2]), reverse=False)

    for e in sorted_list:
        ret += [e[0] for x in range(e[1])]
    return ret


   # return result



print("Example:")
print(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4]))


print(list(frequency_sort([4, 6, 2, 2, 6, 4, 4, 4])))# == [4, 4, 4, 4, 6, 6, 2, 2]
print(list(frequency_sort(['bob', 'bob', 'carl', 'alex', 'bob'])))# == ['bob', 'bob', 'bob', 'carl', 'alex']
print(list(frequency_sort([17, 99, 42])))# == [17, 99, 42]
print(list(frequency_sort([])))# == []
print(list(frequency_sort([1])))# == [1]
print("Coding complete? Click 'Check' to earn cool rewards!")
