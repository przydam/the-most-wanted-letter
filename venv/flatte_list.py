def flat_list(array):
    return_array = []
    for elements in array:
        if isinstance(elements, list):
            elements = str(elements).strip().replace("[", "").replace("]", "").replace(" ", "").split(",")
            return_array.extend(flat_list(elements))
        else:
            if elements != '':
                return_array.append(int(elements))

    return return_array


# def flat_list(array):
#     result = []
#     for el in array:
#         if type(el) != list:
#             result.append(el)
#         else:
#             result.extend(flat_list(el))
#     return result
#





print(flat_list([[[[[[[[[]]]]]]]]]))

print(flat_list([0, 1, 2, 3]))# == [1, 2, 3], "First"
print(flat_list([1, [2, 2, 2], 4]))# == [1, 2, 2, 2, 4], "Second"
print(flat_list([[[2]], [4, [5, 6, [6], 6, 6, 6], 7]]))# == [2, 4, 5, 6, 6, 6, 6, 6, 7], "Third"
print(flat_list([-1, [1, [-2, 1], 1], -1]))# == [-1, 1, -2, 1, -1], "Four"
