import re

VOWELS = "aeiouy"

def translate(phrase):
    result = []

    count = 0
    while count < len(phrase):
        print(phrase[count])
        char = phrase[count]
        result.append(char)

        if char in "aeiouy":
            count += 3
        elif char.isspace():
            count += 1
        else:
            count += 2

    return ''.join(result)




    # #phrase = re.sub(r'([a-z])\1+', r'\1', phrase)
    # #table = VOWELS.split("")
    #
    # licznika = 0
    # licznike = 0
    # liczniki = 0
    # liczniko = 0
    # liczniku = 0
    # liczniky = 0
    #
    #
    #
    #     #phrase = re.match('\ A([aeiouy] + ?) + (? < ! ) \ Z', phrase)
    # while re.search(r'(aa)(.*)\2', phrase):
    #     # print("phrase",phrase)
    #     phrase = re.sub(r'(a)(.*)\1', r'\2', phrase)
    #     print("A phrase", phrase, licznika)
    #     licznika += 1
    #     if (licznika == 0):
    #         phrase = phrase.replace("a", "")
    #     licznika = 0
    # while re.search(r'(ee)(.*)\2', phrase):
    #     phrase = re.sub(r'(e)(.*)\1', r'\2', phrase)
    #     print("E phrase", phrase, licznike)
    #     licznike += 1
    #     if (licznike == 0):
    #         phrase = phrase.replace("e", "")
    #     licznike = 0
    #
    # while re.search(r'(ii)(.*)\2', phrase):
    #     phrase = re.sub(r'(i)(.*)\1', r'\2', phrase)
    #     print("I phrase", phrase, liczniki)
    #     liczniki += 1
    #
    #     if (liczniki == 0):
    #         phrase = phrase.replace("i", "")
    #     liczniki = 0
    #
    # while re.search(r'(oo)(.*)\2', phrase):
    #     phrase = re.sub(r'(o)(.*)\1', r'\2', phrase)
    #     print("O phrase", phrase, liczniko)
    #     liczniko += 1
    #
    #     if (liczniko == 0):
    #         phrase = phrase.replace("o", "")
    #     liczniko = 0
    # while re.search(r'(uu)(.*)\2', phrase):
    #     phrase = re.sub(r'(u)(.*)\1', r'\2', phrase)
    #     print("U phrase", phrase, liczniku)
    #     liczniku += 1
    #
    #     if (liczniku == 0):
    #         phrase = phrase.replace("u", "")
    #     liczniku = 0
    #
    # while re.search(r'(yy)(.*)\2', phrase):
    #     phrase = re.sub(r'(y)(.*)\1', r'\2', phrase)
    #     print("Y phrase", phrase, liczniky)
    #     liczniky += 1
    #
    #     if (liczniky == 0):
    #         phrase = phrase.replace("y", "")
    #     liczniky = 0
    #
    # return phrase


print("Example:")
print(translate("hieeelalaooo"))

#These "asserts" using only for self-checking and not necessary for auto-testing
print(translate("hieeelalaooo"))# == "hello", "Hi!"
print(translate("hoooowe yyyooouuu duoooiiine"))# == "how you doin", "Joey?"
print(translate("aaa bo cy da eee fe"))# == "a b c d e f", "Alphabet"
print(translate("sooooso aaaaaaaaa"))# == "sos aaa", "Mayday, mayday"
print("Coding complete? Click 'Check' to review your tests and earn cool rewards!")
