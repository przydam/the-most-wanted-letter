from typing import List


def checkio(game_result: List[str]) -> str:
    array_help =[]







    for elements in game_result:
        temporary_row_array =[]
        for elements2 in elements:
            temporary_row_array.append(elements2)
        array_help.append(temporary_row_array)



    #print(array_help[1][2])
    #horizontally
    if array_help[0][0] == array_help[0][1] == array_help[0][2] == "O":
        return "O"
    if array_help[0][0] == array_help[0][1] == array_help[0][2] == "X":
        return "X"
    if array_help[1][0] == array_help[1][1] == array_help[1][2] == "O":
        return "O"
    if array_help[1][0] == array_help[1][1] == array_help[1][2] == "X":
        return "X"
    if array_help[2][0] == array_help[2][1] == array_help[2][2] == "O":
        return "O"
    if array_help[2][0] == array_help[2][1] == array_help[2][2] == "X":
        return "X"
    #vertically
    if array_help[0][0] == array_help[1][0] == array_help[2][0] == "O":
        return "O"
    if array_help[0][0] == array_help[1][0] == array_help[2][0] == "X":
        return "X"
    if array_help[0][1] == array_help[1][1] == array_help[2][1] == "O":
        return "O"
    if array_help[0][1] == array_help[1][1] == array_help[2][1] == "X":
        return "X"
    if array_help[0][2] == array_help[1][2] == array_help[2][2] == "O":
        return "O"
    if array_help[0][2] == array_help[1][2] == array_help[2][2] == "X":
        return "X"

    #cross
    if array_help[0][0] == array_help[1][1] == array_help[2][2] == "O":
        return "O"
    if array_help[0][0] == array_help[1][1] == array_help[2][2] == "X":
        return "X"
    if array_help[0][2] == array_help[1][1] == array_help[2][0] == "O":
        return "O"
    if array_help[0][2] == array_help[1][1] == array_help[2][0] == "X":
        return "X"



    return "D"


print("Example:")
print(checkio(["X.O",
                   "XX.",
                   "XOO"]))

    #These "asserts" using only for self-checking and not necessary for auto-testing
print(checkio([
        "X.O",
        "XX.",
        "XOO"]))# == "X", "Xs wins"
print(checkio([
        "OO.",
        "XOX",
        "XOX"]))# == "O", "Os wins"
print(checkio([
        "OOX",
        "XXO",
        "OXX"]))# == "D", "Draw"
print(checkio([
        "O.X",
        "XX.",
        "XOO"]))# == "X", "Xs wins again"
