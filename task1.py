import re



def checkio(text: str) -> str:
    text = text.lower().replace(" ", "")
    text = "".join(re.split("[^a-z]*", text))
    list1 = list(text)
    dictionary = {}

    for lether in list1:
        if lether in dictionary:
            dictionary[lether] += 1
        else:
           dictionary[lether] = 1

    max = list(dictionary.values())[0]
    the_best_lether =[]
    the_best_lether.append(list(dictionary.keys())[0])


    for lether in dictionary:
        if dictionary[lether] == max:
            the_best_lether.append(lether)
        else:
            if dictionary[lether] > max:
                max = dictionary[lether]
                the_best_lether.clear()

                the_best_lether.append(lether)

    the_best_lether.sort()
    return the_best_lether[0]


print("Example:")
checkio("Hello World!")
checkio("Hello World!")
checkio("How do you do?")
checkio("One")
checkio("Oops!")
checkio("AAaooo!!!!")
checkio("abe")
print("Start the long test")
checkio("a" * 9000 + "b" * 1000)
