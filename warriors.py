class Warrior:
    health: int = 50
    attack: int = 5
    is_alive: bool = True

    # def __init__(self):
    #     self.is_alive = self.health > 0


class Knight(Warrior):
    attack = 7


# def fight(copetitor1, copetitor2):
#     round_nr = 0
#     while copetitor1.health >= 0 and copetitor2.health >= 0:
#         round_nr += 1
#         print("Runda ", round_nr)
#         # first hit
#         copetitor2.health -= copetitor1.attack
#         print("Zycie po drugim atakach ")
#         print("Zolniez pierwszy ", copetitor1.health, "Zolniez drugi", copetitor2.health)
#         if copetitor2.health < 0 and copetitor1.health >= 0:
#             return True
#
#         copetitor1.health -= copetitor2.attack
#         print("Zycie po pierwszym atakach ")
#         print("Zolniez pierwszy ", copetitor1.health, "Zolniez drugi", copetitor2.health)
#         if copetitor1.health < 0 and copetitor2.health >= 0:
#             return False
def fight(copetitor1, copetitor2):
    round_nr = 0
    while copetitor1.is_alive and copetitor2.is_alive:
        copetitor2.health -= copetitor1.attack
        copetitor1.health -= copetitor2.attack
        if copetitor2.health <= 0:
            copetitor2.is_alive = False
            return True
        elif copetitor1.health <= 0:
            copetitor1.is_alive = False
            return False


chuck = Warrior()
bruce = Warrior()
carl = Knight()
dave = Warrior()
mark = Warrior()

print("Wynik1 ", fight(chuck, bruce))  # == True
print("Wynik2 ", fight(dave, carl))  # == False
print("Wynik3 ", chuck.is_alive)  # == True
print("Wynik4 ", bruce.is_alive)
print(carl.is_alive == True)
print(dave.is_alive == False)
print(fight(carl, mark))  # == False
print(carl.is_alive == False)
